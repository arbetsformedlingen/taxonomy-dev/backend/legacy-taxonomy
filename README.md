# Data from the legacy taxonomy

The `historical-occupation-names` file contains the old name for old occupation-names.

The `historical-occupation-names-replaced-by` file contains the old occupation-names connection to "newer" legacy occupation-names. So it could be possible to connect these to the Jobtech Taxonomy version 1.

The 'legacy-occupation-to-ssyk' file contains connection between old occupation-name-ids and SSYK. **WANRNING!** The SSYK changed around 2012 so before this the SSYK code meant something differently from today.

See this page https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/  for what codes changed.
